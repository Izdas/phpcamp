<?php
class ServiceFunctions {
	private $pdo;
	public function __construct() {
		$this->pdo = new PDO('mysql:host=localhost;dbname=products;encoding=utf8', 'root', '');
	}
	public function checkProduct($id) {
		// Check if id is set
		if(empty($id))
			throw new Exception('Missing parameter: id');
		
		// Check if id is number
		if(!ctype_digit($id))
			throw new Exception('Wrong data for parameter: id');
		$q = $this->pdo->prepare('SELECT `id`, `name`, `price` FROM `produkt` WHERE `id`=:id LIMIT 1');
		$q->bindValue(':id', $id);
		$q->execute();
		return $q->fetch(PDO::FETCH_ASSOC);
	}
	public function addProduct($name, $price) {
		// Check if name is set
		if(empty($name))
			throw new Exception('Missing parameter: name');
		
		// Check if price is set
		if(empty($price))
			throw new Exception('Missing parameter: price');
		// Check if price is vaild number
		if(!ctype_digit($price) && !preg_match('/^[0-9]+\.[0-9]{2}$/si', $price))
			throw new Exception('Wrong data for parameter: id');
		$q = $this->pdo->prepare('INSERT INTO `produkt` (`name`, `price`) VALUES (:name, :price)');
		$q->bindValue(':name', $name);
		$q->bindValue(':price', $price);
		$q->execute();
		return $this->pdo->lastInsertId();
	}
	public function removeProduct($id) {
		// Check if id is set;
		if(empty($id))
			throw new Exception('Missing parameter: id');
		// Check if id is number
		if(!ctype_digit($id))
			throw new Exception('Wrong data for parameter: id');
		$q = $this->pdo->prepare('DELETE FROM `produkt` WHERE `id`=:id LIMIT 1');
		$q->bindValue(':id', $id);
		$q->execute();
		return $this->pdo->rowCount();
	}
	
}
$server = new SoapServer(NULL, ['uri'=>'http://localhost']);
$server->setClass('ServiceFunctions');
$server->handle();